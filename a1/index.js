/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
    //function declaration and nested function expression
    function promptName() {
        let fullName = prompt("Hi, what is your full name?");
        console.log("Hello, " + fullName);

        let age = prompt("What is your age?");
        console.log("You are " + age + " years old.");

        let location = prompt("What is your location?");
        console.log("You live in " + location);
        alert("Thank You!");
    }
    promptName();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
    //function declaration and array inside
    function favoriteMusicArtist() {
        let myMusicArtist = ["1. Bee Gees", "2. Ariana Grande", "3. Hanae", "4. Kumiko Kaori", "5. Ritsuki Nakano"];
        console.log("My favorite Music Artist:");
        console.log(myMusicArtist[0]);
        console.log(myMusicArtist[1]);
        console.log(myMusicArtist[2]);
        console.log(myMusicArtist[3]);
        console.log(myMusicArtist[4]);
    }
    favoriteMusicArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
    //function declaration and array inside
    function favoriteMovies() {
        console.log("My favorite Movies:");

        let myMoviesOne = ["1. The sea beast", "94%"];
        console.log(myMoviesOne[0]);
        console.log("Rotten Tomatoes Rating: " + myMoviesOne[1]);

        let myMoviesTwo = ["2. Turning red", "95%"];
        console.log(myMoviesTwo[0]);
        console.log("Rotten Tomatoes Rating: " + myMoviesTwo[1]);

        let myMoviesThree = ["3. Spirited away", "97%"];
        console.log(myMoviesThree[0]);
        console.log("Rotten Tomatoes Rating: " + myMoviesThree[1]);

        let myMoviesFour = ["4. Encanto", "91%"];
        console.log(myMoviesFour[0]);
        console.log("Rotten Tomatoes Rating: " + myMoviesFour[1]);

        let myMoviesFive = ["4. Coco", "97%"];
        console.log(myMoviesFive[0]);
        console.log("Rotten Tomatoes Rating: " + myMoviesFive[1]);
    }
    favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/
//function expression
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2);
	console.log(friend3); 
};
printFriends();